//classe Deck contenant des pions reparti en 2 catégories: la pioche aléatoire (contenant 

public class Deck {
	
	//attribut
	private int joueur; 
	private int reste; //nombre de pions restants dans le deck connu
	private Pion[] deck;
	private int choix;
	
	public Deck (int j, int taille, int choix) {
		this.joueur=j; 
		this.choix=choix;
		this.reste=choix;
		this.deck = new Pion [taille];
		genererDeckalea();
		genererDeckconnu(); 
	}
	public void genererDeckconnu () {
			for (int i=0; i<this.choix; i++) {
				if (i<choix-3) {
					this.deck[i] = new Pion (this.joueur, 1);
				}else{
					if(i<choix-2){
						this.deck[i]=new Pion(this.joueur,2);
					}else {
						this.deck[i]=new Pion(this.joueur,3);
					}
				}
			}
			
	}
	public void genererDeckalea () {
		Pion n= new Pion (joueur,1);
		Pion m=new Pion(joueur, 2);
		Pion o=new Pion(joueur,3);
		double a;
		for (int i=this.choix; i<this.deck.length; i++) {
			a=Math.random();
			
			if (a<0.05) {
					deck[i]=o;
				}
			else{ 
				if(a<0.2) {
					deck[i]=n;
				}else{
					deck[i]=m;
				}
			}
	}
	}
	//affichage du type partie connue ponderée + partie pioche aléatoire
	public void afficherDeck () {
		int nbtype=3; //nombre de type
		for (int i=1; i<nbtype+1; i++) {
			System.out.println ("type de pion : " + i + "  nb restant : " + compterPion(i));
			descriptionType(i);
		}
		System.out.println("pioche aleatoire");
	}
	
	//methode décrivant les différents type de pion pour faciliter le choix du joueur
	public void descriptionType( int type) {
		switch (type) {
			case 1 :
				System.out.println("\u001B[36m Pion normal, sans effet particulier \u001B[0m");
				break;
			case 2 :
				System.out.println("\u001B[36m Pion bombe, explose les pions à droite, en bas et à gauche de lui (et lui même) \u001B[0m");
				break;
			case 3 :
				System.out.println("\u001B[36m Pion inverse, inverse sa position avec celle du pion en dessous de lui \u001B[0m");
				break;
		}
	}
	public Pion piocheConnu ( int type) {
		//Parcourir la pioche connue du deck jusqu'à trouver un pion du type pour pouvoir l'enlever
		int i=0;
		boolean presence=false; // verifie qu'il y a bien un pion restant de ce type
		Pion p = new Pion (this.joueur, type);
		while ((i<this.reste)&&(presence==false)) {
			  
			 if(this.deck[i]!=null && deck[i].getType()==type){
			 enleverPion(i); 
			 presence=true;
			 }
			 i++;
		}
		if (presence==false) {
			p=null;
			System.out.println("il n'y a plus de pion de ce type");
			}
		  	
		return p;
		}
	
	//piocher le premier pion de la pioche aléatoire, pion situe à la case en dessous du dernier connu, donc à la position reste.	
	public Pion piocheAlea () {
		Pion p= this.deck[choix];
		enleverPion(this.choix);
		return p;
			}	
	public int compterPion(int type) {
		int cpt =0; //compteur du nombre de pion de ce type
		
		for (int i=0; i<this.reste; i++) {
			if  ((this.deck[i]!=null)&& this.deck[i].getType()==type) {
				cpt++;
			}
		}
		return cpt;
	}

	/* getteurs pour les attributs */
	
	public Pion[] getDeck () {
		return deck ;
	}
	
	public int getChoix () {
		return this.choix;
	}
	public int getJoueur() {
		return this.joueur;
	}
	public int getReste () {
		return this.reste;}
	
	//methode enlevant le pion choisi (ou non) de la pioche 
	public void enleverPion (int pos) {
		
		if (pos<this.choix) {   //decaler les lignes contenant les pions vert le haut 
			this.reste= this.reste-1;
			this.deck[pos] = null;
		for (int i=pos+1; i<reste; i++) {
			deck[i-1] = deck[i];
			}
		    deck[reste]=null;
		}else { 
			deck[deck.length-1] = null;
			
				for (int i=pos+1; i<deck.length; i++) {
					deck[i-1] = deck[i];}
				}
		}
	}
			


