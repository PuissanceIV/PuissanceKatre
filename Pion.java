public class Pion{
	
	private int Proprio;
	private int Type;
	
	public Pion (int Joueur, int type){
		this.Proprio = Joueur;
		this.Type=type;
	}
		
	public int getJoueur(){
		return this.Proprio;
	}
		
	public int getType(){
		return this.Type;
	}
	
	public void Effet_Spec(int X,Plateau grille){
		switch (this.Type){
			case 1:
				//effet 1 (pas d'effet)
				break;
			case 2:
				//effet 2: bombe, explose 3 poins (cotes + dessous)
				int Y = grille.getHauteur(X);
				grille.delPion(X,Y);
				grille.delPion(X-1,Y);
				grille.delPion(X+1,Y);
				grille.delPion(X,Y+1);
				break;
			case 3:
				//effet 3
				break;
			
		}
		
		
	}
		
	
	
	
}
