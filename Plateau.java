public class Plateau {

	Pion p ; // instance de la classe Pion
	Pion [][] monde ; // tableau représentant le jeu

	public Plateau(){
		this.monde = new Pion[6][7];	// 6 lignes	et 7 colonnes		
	}


	public boolean verifJouable(int PosX){		//Vérifie si une colonne est complète ou non
		boolean verif = true;
		if(monde[0][PosX] != null) 				
			{verif = false;
				}
		return verif;
	}

//Methode donnant la ligne où le pion de pose selon s'il y a deja un pion en dessous ou pas
	public int getHauteur(int PosX){
		int PosY = 0;
		
		while ((PosY+1<6)&&(this.monde[PosY+1][PosX] == null) && (PosY<monde.length-1)){
				
				PosY++;
			}
		
	return PosY;
	}	
		
	public void setPion(int PosX, Pion p){
		this.monde[getHauteur(PosX)][PosX]= p;
	}
	
	public void delPion(int X, int Y){  //suppression de pions
		if(X==5){
			System.out.println(" La bombe n'a pas explose");
		}
		else if(X<5 && Y<7 && X>=0 && Y>=0){
			this.monde[X][Y]= null;
		}
		}
		
		public void gravite(){
		for(int i = 0; i<7; i++){//colonnes
			for(int j = 5; j>0;j--){//lignes
				if(this.monde[j][i]==null && this.monde[j-1][i]!=null){
					this.monde[j][i]=this.monde[j-1][i];
					this.monde[j-1][i]=null;
				}
			}	
		}	
	}
	
	public void inverse(int Y, int X){
		if(Y<5){
			Pion transporteur = this.monde[Y][X];
			this.monde[Y][X]  = this.monde[Y+1][X];
			this.monde[Y+1][X]= transporteur;
		}
	}	
		
		
		
		
		
				
	public boolean detectionVictoire(int PosX, int Joueur){
		
		boolean Victoire = false;
		int i=0;
		int j=0;
		
		// POUR SIMPLIFIER :
		// i : Curseur x
		// j : curseur y
		
		// LIGNE
		
		i = PosX;
		int Compteur = 0;
		
		// A DROITE
		while(i<7 && monde[getHauteur(PosX)+1][i]!=null && monde[getHauteur(PosX)+1][i].getJoueur() == Joueur  ){ 
			Compteur++;
			i++;
		}
		
		
		i = PosX;

		
		//A GAUCHE
		while(i>=0 && monde[getHauteur(PosX)+1][i]!=null && monde[getHauteur(PosX)+1][i].getJoueur() == Joueur ){
			Compteur++;
			i--;
		}
		
		if(Compteur >= 5)Victoire = true; //5 et non 4 car le pion posé est compté deux fois dans tous les cas de figures possibles.
		//System.out.println("Compteur Ligne = "+Compteur);
		
		// COLONNE
		
		j=getHauteur(PosX)+1;
		Compteur = 0;
		
		// EN HAUT
		while(j>=0 && monde[j][PosX]!=null && monde[j][PosX].getJoueur() == Joueur){	// Balayage en colonne en haut. 
			Compteur++;
			j--;
		}
		
		j=getHauteur(PosX)+1;

		
		// EN BAS
		while(j<6 && monde[j][PosX]!=null && monde[j][PosX].getJoueur() == Joueur  ){
			Compteur++;
			j++;
		}
		
		if(Compteur >= 5)Victoire = true;
		//System.out.println("Compteur Colonne = "+Compteur);
		
		// DIAGONALES
		
		
		Compteur = 0;
		i = PosX;
		j = getHauteur(PosX)+1;
		
		// DIAGONALE \
		// EN BAS A DROITE (i++ j++)
		while(i<7 && j<6 && monde[j][i]!=null && monde[j][i].getJoueur() == Joueur ){ // Balayage en bas à droite
			Compteur++;
			i++;
			j++;
		}


		i = PosX;
		j = getHauteur(PosX)+1;
		
		// EN HAUT A GAUCHE (i-- j--)
		while(i>=0 && j>=0 && monde[j][i]!=null && monde[j][i].getJoueur()==Joueur){ 
			Compteur++;
			i--;
			j--;
		}
		
		if(Compteur >= 5)Victoire = true;
		//System.out.println("Compteur antiSlash = "+Compteur);
		
		Compteur = 0;
		i = PosX;
		j = getHauteur(PosX)+1;
		
		// DIAGONALE /
		// EN HAUT A DROITE (i++ j--)	
		while(i<7 && j>=0 && monde[j][i]!= null && monde[j][i].getJoueur() == Joueur ){
			Compteur ++;
			i++;
			j--;
		}
		
		i = PosX;
		j = getHauteur(PosX)+1;	
		
		// EN BAS A GAUCHE (i-- j++)
		while(i>=0 && j<6 && monde[j][i]!=null && monde[j][i].getJoueur()==Joueur ){
			Compteur++;
			i--;
			j++;
		}
		
		if(Compteur >= 5)Victoire = true;
		//System.out.println("Compteur / = "+Compteur);
		

		
		
		return Victoire;
	}

	
	
	}
	
