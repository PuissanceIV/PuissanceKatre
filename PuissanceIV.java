import java.util.Scanner;

public class PuissanceIV{
    
    public static void main (String[] Argz){
    Scanner s= new Scanner(System.in);
    //creation du jeu
    Plateau grille = new Plateau();
    Ecran ecran=new Ecran (6, 7);
   
    
    Deck D1 = new Deck(1,21,10);   //int joueur, int taille(nombre de pions), int choix(nombre max de pions choisis)
    
    Deck D2 = new Deck (2,21,10);
    //variables et debut de partie 
    int j=2;  // premier joueur
    Deck deck = D2;  //premier deck
    ligne();

	int pos=0; //colonne du pion joué
	
    //tour de jeu 
        
       do  {
		    j=changementJoueur(j);
		   if(deck==D1) deck=D2;
		   else { deck=D1; }

		  Pion p=choixDuPion(deck,j,ecran);
          ecran.rafraichir(grille);
          pos=placerPion(p,grille);
          ecran.rafraichir(grille);
          
         } while (grille.detectionVictoire(pos,j)==false);
   ligne();
   System.out.println("Bravo! Vous avez gagné");
    
}


    public static Pion choixDuPion (Deck d, int j, Ecran ecran){
        Scanner s= new Scanner(System.in);
        //creation d'un pion initial
        Pion pion=null; 
        System.out.println ("Choississez un pion à jouer dans le Deck suivant ");
        ligne();
        ecran.deck(d);
        ligne();
        int a;
        if (d.getReste()==0){
			a=2;}
		else{
			System.out.println("Pour prendre un pion connu tapez 1, sinon tapez 2");
				a =s.nextInt();
			while ((a!=1)&&(a!=2)) {
				System.out.println("le numero ne correspond pas, veuillez rentrer un autre numéro");
				a=s.nextInt();
			}
		}
        
		if (a==1){ //choix d'un pion dans le deck connu
			int type=0;
			while(pion==null){ 
				System.out.println("choississez le type de pion parmi ceux du deck");
		
				type= s.nextInt();
				pion= d.piocheConnu(type);
			}
            
            
		}else {
			pion = d.piocheAlea();
			System.out.println("le pion aléatoire que vous avez pioché est de type " + pion.getType());
			d.descriptionType(pion.getType());
		}
			
            return pion;
        }
        
   public static int placerPion (Pion p, Plateau grille)  {
       Scanner s = new Scanner(System.in); 
       //choix de la colonne
       System.out.println("Choissisez le numero de la colonne pour placer le pion");
       int colonne =s.nextInt();
		
		application (colonne, grille, p);
		//placement du pion
      
       return colonne;
       
   }  
   
   public static void application(int colonne, Plateau grille, Pion p) {
	   //verification possibilité de poser le pion
	   Scanner s = new Scanner(System.in);
	   while(grille.verifJouable(colonne) ==false){
		System.out.println("la colonne est pleine, choississez une nouvelle colonne");
		colonne=s.nextInt();
		 }  
	   grille.setPion(colonne,p);
       //Effet scpecial
       switch (p.getType()
       ){
	 		case 1:
				//effet 1 (pas d'effet)
				break;
			case 2:
				//effet 2: bombe, explose 3 poins (cotes + dessous)
				int Y = grille.getHauteur(colonne)+1;
			    grille.delPion(Y,colonne);
				grille.delPion(Y,colonne-1);
				grille.delPion(Y,colonne+1);
				grille.delPion(Y+1,colonne);
				grille.gravite();
				break;
			case 3:
				//effet 3
				//effet 3: echange de place avec le pion sur lequel il tombe
				grille.inverse(grille.getHauteur(colonne)+1,colonne);
				break;

				
			
		}
     
       
   }  
   public static int changementJoueur(int j) {
       if (j%2==0){
           j=1;}
        else { j=2;
       }
       ligne();
       System.out.println("c'est au tour du joueur "+j+" de jouer" ); 
       return j; 
   }
   
   public static void ligne() {
       
       System.out.println("" ); 
       
   }
   
   
   
   
  
   
   
   
   
    }
